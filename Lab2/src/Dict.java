import java.io.*;
import java.lang.*;
import java.util.*;


public interface Dict {
  // save: guarda el diccionario (sea formato esp-ing o ing-esp) en la forma esp-ing
  public void save(String path, int r);

  // imprime: imprime el diccionario que se tiene (en la forma que este ya que es para debuging)
  public void print();

  // loadesp: devuelve un diccionario de la forma palabraesp,palabraing
  public Dict loadEspanish(String path);

  // loading: devuelve un diccionario de la forma palabraing,palabraesp
  public Dict loadInglish(String path);

  // agregadict: agrega (palabra,traduccion) al diccionario
  public void add(String palabra1, String palabra2);

  // buscadict: devuelve si una palabra esta en el diccionario
  public boolean find(String palabra1);

  // traduccion: devuelve la traduccion de una palabra que esta en el diccionario
  public String get(String palabra1, int r);
}
