import java.io.*;
import java.util.*;
import java.util.Set;
import java.util.Map;
import java.util.Iterator;


// clase dict que extiende a treemap con un metodo para cargar diccionarios de file.
public class Dictmap implements Dict {

  private TreeMap<String, String> palabras;


  public Dictmap() {
    TreeMap<String, String> listapalabras = new TreeMap<String, String>();
    palabras = listapalabras; 
  } 


  //dict esp from file 
  public Dict loadEspanish(String path) {
    //Lista dict = newDictmap()) ;
    Dictmap dict = new Dictmap();
    TreeMap<String, String> listapalabras = new TreeMap<String, String>();
    dict.palabras = listapalabras;
    FileReader input = null;
    try {
      input = new FileReader(path);
      int caracter = input.read();

      String palabra = "";
      String palabra1 = "";
      String palabra2 = "";
      while (caracter != -1) {
        // 44 es , 10 enrter 32 espacio
        if (caracter == 10 || caracter == 44) {
          if (caracter == 44) {
            palabra1 = palabra.replace(",", "");
            //System.out.println(palabra1);
            palabra = "";
          } else if (caracter == 10) {
            palabra2 = palabra.replace("\n", "");
            //System.out.println(palabra2);
            palabra = "";
            dict.palabras.put(palabra1, palabra2);
          } else {
            palabra = "";
          }
        } else {
          palabra = palabra + (char)caracter;
        }
        caracter = input.read();
      }
    }
    catch (Exception e) {
      return (Dict)dict ;
    } finally {
      // En el finally cerramos el fichero, para asegurarnos
      // que se cierra tanto si todo va bien como si salta 
      // una excepcion.
      try {                    
        if (null != input) {   
          input.close();     
        }                  
      } catch (Exception e2) { 
        e2.printStackTrace();
      }
    }
    return (Dict)dict;
  }


  //dict ing from file 
  public Dict loadInglish(String path) {
    //Lista dict = newDictmap()) ;
    Dictmap dict = new Dictmap();
    TreeMap<String, String> listapalabras = new TreeMap<String, String>();
    dict.palabras = listapalabras;
    FileReader input = null;
    try {
      input = new FileReader(path);
      int caracter = input.read();

      String palabra = "";
      String palabra1 = "";
      String palabra2 = "";
      while (caracter != -1) {
        // 44 es , 10 enrter 32 espacio
        if (caracter == 10 || caracter == 44) {
          if (caracter == 44) {
            palabra1 = palabra.replace(",", "");
            palabra = "";
          } else if (caracter == 10) {
            palabra2 = palabra.replace("\n", "");
            palabra = "";
            dict.palabras.put(palabra2, palabra1);
          } else {
            palabra = "";
          }
        } else {
          palabra = palabra + (char)caracter;
        }
        caracter = input.read();
      }
    } catch (Exception e) {
      return (Dict)dict ;
    } finally {
      // En el finally cerramos el fichero, para asegurarnos
      // que se cierra tanto si todo va bien como si salta 
      // una excepcion.
      try {                    
        if (null != input) {   
          input.close();     
        }                  
      } catch (Exception e2) { 
        e2.printStackTrace();
      }
    }
    return (Dict)dict;
  }


  public void add( String palabra1, String palabra2) {
    palabras.put(palabra1, palabra2) ;
  }


  public boolean find( String palabra1) {
    boolean p  = palabras.containsKey(palabra1);
    return p ;
  }


  public void save( String path, int r) {
    FileWriter output = null;
    try {   
      output = new FileWriter(path);
      for (Map.Entry<String,String> entry : palabras.entrySet()) {
        String key = entry.getKey();
        String value = entry.getValue();
        // se traduce de español a ingles, y debe ser llamado con
        //el diccionario español ingles
        if (r == 0) { 
          output.write(key + "," + value + "\n");
        // se traduce de español a ingles
        //por lo que el diccionario dict que estamos procesando esta alreves
        // por lo que hay que guardarlo alreves
        } else {
          output.write(value + "," + key + "\n");
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      // En el finally cerramos el fichero, para asegurarnos
      // que se cierra tanto si todo va bien como si salta 
      // una excepcion.
      try {                    
        if (null != output) {   
          output.close();     
        }                  
      } catch (Exception e2) { 
        e2.printStackTrace();
      }
    }
  }


  // no se usa variable r en esta funcion, pero en esta funcion de la otra
  // implementacion si, por eso esta.
  public String get( String palabra1, int r) {
    return palabras.get(palabra1);
  }


  public void print() {
    for (Map.Entry<String,String> entry : palabras.entrySet()) {
      String key = entry.getKey();
      String value = entry.getValue();
      System.out.print(key + "," + value + "\n");
    } 
  }
}
