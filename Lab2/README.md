---------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------
# Traductor 

## Introduccion
* Se implementa un traductor en Java para traducir documentos en ingles y español. El usuario ingresa un documento de entrada, un diccionario bilingue, y un diccionario de palabras a ignorar; y el programa devuelve un archivo con su traduccion, si una palabra del documento no está en los diccionarios entonces se le ofrece al usuario: ignorar dicha palabra, ignorarla siempre (agregándola al diccionario de palabras ignoradas), traducirla de una forma en todo el documento o traducrila de una forma en todo el documento y a demás agregarla al diccionario bilingue. A demás, se permite traducir de ingles a español si el usiario ingresa la opcion reverse. Y tambien se le permite elegir por medio del 
parámetro -m, la interfaz de diccionario que usará el traductor.

## Cómo funciona ?
* (requerido) El usuario ingresa un documento a traducir por linea de comando bajo el comando -i o --input=
* Opcionalmente el usuario puede ingresar alguno de los siguientes objetos: Un archivo diccionario bilingue de palabras en español e ingles, un archivo diccionario o lista de palabras que el traductor debe ignorar, un archivo donde
se guardara el documento traducido, un parametro -r para indicar si se debe traducir un documento en la lengua inglesa o española, y un parametro -m seguido de 1 o 0 para indicar bajo que interfaz de diccionario funcionara el traductor.
*Una vez iniciada la ejecucion, el programa controla que el documento de entrada esté, luego prosigue a procesar el resto de los argumentos si es que los hubiera. A partir de estos, construye los siguientes objetos:
- dictesp: un diccionario bilingue de la forma palabra española,palabra inglesa que se utilizara para buscar las traducciones de las plabaras del Documento de entrada y para la funcionalidad "Traducir siempre como"
- dictespaux: un diccionario bilingue vacio de la forma palabra española,palabra inglesa que se utilizara para la funcionalidad "Traducir como"
- dicting: un diccionario bilingue de la forma palabra inglesa,palabra española que se utilizara para buscar las traducciones de las plabaras del Documento de entrada y para la funcionalidad "Traducir siempre como"
- dictingaux: un diccionario bilingue vacio de la forma palabra inglesa,palabra española que se utilizara para la funcionalidad "Traducir como"
- dictignored: un diccionario de palabras ignoradas para implementar la funcionalidad "h ignorar siempre" y para verificar si hay que ignorar las palabras del documento de entrada
- traductor: un objeto que utilizando los anteriores objeto traduce el documento
- doc: el documento de entrada
- y otro documento que representa el documento traducido
*Una vez creados los anteriores objetos, se prosigue a la traduccion. El objeto doc almacena una lista de Strings donde cada uno de estos Strings es una oracion del documento de entrada. El traductor va procesando cada palabra
de cada elemento de la lista, y tal proceso consiste en lo siguiente:
-Si la palabra a traducir está en el diccionario de palabras ignoradas entonces la traduccion es dicha palabra.
-En cambio, si la palabra a traducir está en el diccionario auxiliar bilingue la traduccion es la traduccion que aparece en tal diccionario 
-En cambio, si la palabra a traducir está en el diccionario bilingue no auxiliar entonces la traduccion es la traduccion que aparece en tal diccionario 
-Sino, se le da la posibilidad al usuario de ignorar una vez la palabra por lo que la traduccion es tal palabra; o de ignorarla siempre, cumpliendo la anterior funcionalidad y agregando la palabra
al diccionario de palabras ignoradas; o de traducrila como, por lo que el usuario ingresa por linea de comando una traduccion que traducirá tal palabra en todas las ocurrencias de la misma en el documento;
o por último, de traducirla siempre como, que a demás de la anterior funcionalidad, agrega la traduccion al diccionario bilingue no auxiliar. 

## Decisiones de diseño:
*  Se utilizan diccionarios auxiliares para guardar las palabras bajo la funcionalidad "t-- Traducir como"
*  Se cargan dos diccionarios a partir del diccionario dado como argumento, uno de la forma palabra española-inglesa para ser utilizado si el flag reverse ha sido activado, y otro de la forma palabra inglesa-española
para ser utilizado si el flag reverse no ha sido activado. Vale decir que si el diccionario bilingue original sufre alguna modificación, el diccionario resultante tiendá el mismo formato que el orignial, es decir,
palabra española-palabra inglesa, independientemente de la activacion del flag -r. 
* Los archivos .txt deben estar en el directorio donde se ubica src, lib, bin.

## Instalacion
Abrir una terminal y situarse en el directorio donde se encunetra bin src y lib. Ejecutar make y luego "java -cp bin:lib/* Main -i input.txt -d dict.txt -g ignored.txt -o output.txt" (con -r o -m  si se lo desea).


---------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------