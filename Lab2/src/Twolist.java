import java.io.*;
import java.util.*;
import java.util.Set;


public class Twolist implements Dict {
  private ArrayList<String> palabrasesp;
  private ArrayList<String> palabrasing;


  //la idea de esta implementacion de diccionario es que un diccionario es dos
  //listas de mismo tamaño,
  //donde una representa las palabras en español y la otra las palabras en ingles,
  //asi palabraing[i]  es la traduccion en ingles de palabraesp[i], y juntas son la i-esima tupla
  //(palabra-traduccion) del diccionario.

  public Twolist() {
    ArrayList<String> listaesp = new ArrayList<String>();
    ArrayList<String> listaing = new ArrayList<String>();
    palabrasesp = listaesp;
    palabrasing = listaing;
  }


  public void save(String path, int r) {
    FileWriter output = null;
    int i = 0;
    try {
      output = new FileWriter(path);
      while (i < palabrasesp.size()) {
        String key = palabrasesp.get(i);
        String value = palabrasing.get(i);
        // se traduce de español a ingles, y debe ser llamado con
        //el diccionario español ingles
        if (r == 0) { 
          output.write(key + "," + value + "\n");
        } else {
          // se traduce de español a ingles
          // por lo que el diccionario dict que estamos procesando esta alreves
          // por lo que hay que guardarlo al reves
          output.write(value + "," + key + "\n");
        }
        i++;
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      // En el finally cerramos el fichero, para asegurarnos
      // que se cierra tanto si todo va bien como si salta 
      // una excepcion.
      try {
        if(null != output) {
          output.close();
        }
      } catch (Exception e2) {
        e2.printStackTrace();
      }
    }
  }

  public void print() {
    int i = 0;
    while (i < palabrasesp.size()) {
      System.out.println(palabrasesp.get(i) + "," + palabrasing.get(i)) ;
      i++;
    }
  }

  //dict esp from file 
  public Dict loadEspanish(String path) {
    Twolist dict = new Twolist();
    FileReader input = null;
    try {
      input = new FileReader(path);
      int caracter = input.read();
      String palabra = "";
      String palabra1 = "";
      String palabra2 = "";
      while (caracter != -1) {
        // 44 es , 10 enrter 32 espacio
        if (caracter == 10 || caracter == 44) {
          if (caracter == 44) {
            palabra1 = palabra.replace(",", "");
            palabra = "";
          } else if (caracter == 10) {
            palabra2 = palabra.replace("\n", "");
            palabra = "";
            dict.palabrasesp.add(palabra1);
            dict.palabrasing.add(palabra2);
          } else {
            palabra = "";
          }
        } else {
          palabra = palabra + (char)caracter;
        }
        caracter = input.read();
      }
    } catch (Exception e) {
      return (Dict)dict ;
    } finally {
      // En el finally cerramos el fichero, para asegurarnos
      // que se cierra tanto si todo va bien como si salta 
      // una excepcion.
      try {
        if (null != input) {
          input.close();
        }
      } catch (Exception e2) {
        e2.printStackTrace();
      }
    }
    return (Dict)dict;
  }


  //dict ing from file 
  public Dict loadInglish(String path) {
    Twolist dict = new Twolist();
    FileReader input = null;
    try {
      input = new FileReader(path);
      int caracter = input.read();
      String palabra = "";
      String palabra1 = "";
      String palabra2 = "";
      while (caracter != -1) {
        // 44 es , 10 enrter 32 espacio
        if (caracter == 10 || caracter == 44) {
          if (caracter == 44) {
            palabra1 = palabra.replace(",", "");
            palabra = "";
          } else if (caracter == 10) {
            palabra2 = palabra.replace("\n", "");
            palabra = "";
            dict.palabrasesp.add(palabra2);
            dict.palabrasing.add(palabra1);
          } else {
            palabra = "";
          }
        } else {
          palabra = palabra + (char)caracter;
        }
        caracter = input.read();
      }
    } catch (Exception e) {
      return (Dict)dict ;
    } finally {
      // En el finally cerramos el fichero, para asegurarnos
      // que se cierra tanto si todo va bien como si salta 
      // una excepcion.
      try {
        if (null != input) {
          input.close();
        }
      } catch (Exception e2) {
        e2.printStackTrace();
      }
    }
    return (Dict)dict;
  }


  public void add(String palabra1, String palabra2) {
    
    palabrasesp.add(palabra1);
    palabrasing.add(palabra2);
  }


  public boolean find(String palabra1) {
    
    int i = 0;
    boolean p = false;
    while (i < palabrasesp.size()) {
      if (palabrasesp.get(i).equals(palabra1)) {
        p = true;
        return p;
      }
      if (palabrasing.get(i).equals(palabra1)) {
        p = true;
        return p;
      }
      i++;
    }
    return p;
  }


  public String get(String palabra1, int r) {
    
    // traduccion de esp a ingles
    String traduccion = "";
    int i = 0;
    if (r == 1) {
      while (i < palabrasesp.size()) {
        if (palabrasesp.get(i).equals(palabra1)) {
          traduccion = palabrasing.get(i);
          return traduccion;
        }
        i++;
      }
    } else {
      while (i < palabrasing.size()) {
        if (palabrasesp.get(i).equals(palabra1)) {
          traduccion = palabrasing.get(i);
          return traduccion;
        }
        i++;
      }
    }
    return traduccion;
  }


}
