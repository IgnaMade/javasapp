
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.io.FileNotFoundException;

public class Document {

	private ArrayList<String> oraciones ;

	// Documento se representa como una lista de oraciones
	public Document(){
		ArrayList<String> listaoraciones = new ArrayList<String> ();
		oraciones = listaoraciones;
	}

   // se carga documento desde el path dado por el usuario
	public Document load(String path) {
		int caracter = 0;
		String oracion = "";
		FileReader input = null;
		try {
			input = new FileReader(path);
			// sino se enontro el documento, se devuelve null y mensaje de alerta
		} catch (FileNotFoundException e) {
			System.out.print("Not Found Input Document \n");
			return null;
		}
		try {
			caracter = input.read();
		} catch (IOException e) {
		}
		while(true) {
			if ((caracter == 10) || (caracter == -1)) {
				if (caracter == -1  && oracion == ""){
					break;
				}
				else if (caracter == -1  && oracion != ""){
					 this.addOr(oracion);
					oracion = "";
					break;
				}
				else if (caracter == 10  && oracion != "") {
					oracion = oracion + (char)caracter;
					this.addOr(oracion);
					oracion = "";
					try {
						caracter = input.read();
					} catch (IOException e) {
					}
				}
				else{
					break;
				}
			}
			else {
				oracion = oracion + (char)caracter;
				try {
					caracter = input.read();
				} catch (IOException e) {
				}
			}
		}
		try {
			input.close();
		} catch (IOException e) {
		}
		return this ;
	}

	public ArrayList<String> listOr() {
		return oraciones ;
	}

	// agrega oracion
	public void addOr(String linea ) {
		oraciones.add(linea) ;
	}

	public void print() {
		int i = 0;
		while (i < oraciones.size()) {
			System.out.print(oraciones.get(i));
			i++;
		}
	}

	public void save( String path) {
		FileWriter output = null;
		try{   
			output = new FileWriter(path);
			int i = 0;
			while (i < oraciones.size()) {
				output.write(oraciones.get(i));
				i++;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}finally{
			// En el finally cerramos el fichero, para asegurarnos
			// que se cierra tanto si todo va bien como si salta 
			// una excepcion.
			try{                    
				if( null != output ){   
					output.close();     
				}                  
			}catch (Exception e2){ 
				e2.printStackTrace();
			}
		}
	}


}