import java.io.*;
import java.util.*;


public class Mainhelper {

  private String inputPath;
  private String outputPath;
  private String dictPath;
  private String ignoredPath;
  private int r;
  private int m;


 // Clase que modulariza la clase Main, para que esta tenga menos responsabilidades
  public Mainhelper() {

  }

  // devuelve el flag de la interfaz si lo ingreso el usuario
  public int returnInterface() {
	  return m; 
  }

  // devuelve el flag reverse si lo ingreso el usuario
  public int returnReverse() {
    return r;
  } 

  // Carga el documento a traducir a partir del path dado por 
  // el usuario
  public Document inputFromPath() {
    Document doc = new Document();
    doc = doc.load(inputPath);
    
    return doc;
  }

  // Carga el diccionario de palabras ignoradas a partir, del path dado,
  // si el usuario no ingreso el path se utiliza un diccionario vacio
  // ignorednuevo.txt como diccionario de palabras ignoradas
  public Ignored ignoredFromPath() {
    Ignored dictignored = new Ignored();
     if (ignoredPath != null) {
       dictignored = dictignored.load(ignoredPath);
     } else{
         inputPath = "ignorednuevo.txt" ;
     }
     return dictignored; 
  }

  // Carga el diccionario bilingue, a partir del path dado y de la version,
  // ingresasa por el usuario
  // si el usuario no ingreso el path se utiliza un diccionario vacio
  // dictnuevo.txt como diccionario bilingue.
  public Dict dictFromPath(int version) {
    Dict dict = null;
    if (m == 1) {
      dict = new Dictmap();
    }
    else {
      dict = new Twolist();
    }
     if (dictPath == null) {
      dictPath = "dictnuevo.txt";
     } else {
      // devuelve el diccionario palabra española,palabra inglesa
      if (version ==1) {
        dict = dict.loadEspanish(dictPath); 
      }
      // devuelve el diccionario palabra española,palabra inglesa
      else if (version ==0) {
        dict = dict.loadInglish(dictPath);
      }
     }
     return dict; 
  }

  //guarda el documento traducido, y los diccionarios
  public void save(Document doc, Ignored dictIgnored, Dict dictEsp, Dict dictIng) {
    doc.save(outputPath);
    dictIgnored.save(ignoredPath);
    if (r == 1) {
      dictEsp.save(dictPath,0);
    }
    else {
      dictIng.save(dictPath,1);
    }
  }

  // procesa los argumentos de consola y devuelve, los path de los archivos
  // de los comandos pasados por el usuario por consola
  public Mainhelper readCommands(Menu settings) {
      // si el unico argumento requierido no esta devuelve null (el argumento es el documento input)
      if (settings.inputpath == null && settings.inputpath2 == null) {
        System.out.print("input requerida \n") ;
        return null;
      }
    if (settings.inputpath2 != null) {
      settings.inputpath = settings.inputpath2 ;
    }
     if (settings.outputpath2 != null) {
      settings.outputpath = settings.outputpath2 ;
    }
     if (settings.ignoreddictpath2 != null) {
      settings.ignoreddictpath = settings.ignoreddictpath2 ;
    }
     if (settings.dictpath2 != null) {
      settings.dictpath = settings.dictpath2 ;
    }
    int reverse = 0 ;
    if (settings.reverse != null || settings.reverse2 != null ) {
        reverse = 1 ;
      }
    int model = 0;
    if (settings.interfaz != null) {
      assert (settings.interfaz == 0 || settings.interfaz == 1) ;
      model = (int)settings.interfaz ;         
    }
    else if (settings.implementation != null) {
      assert (settings.implementation == 0 || settings.implementation == 1) ;
      model = (int)settings.implementation ;           
    }  
    inputPath = settings.inputpath;
    outputPath = settings.outputpath;
    dictPath = settings.dictpath;
    ignoredPath = settings.ignoreddictpath;
    r = reverse;
    m = model;
  return this;    
  }


}