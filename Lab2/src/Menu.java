import com.beust.jcommander.*;
import java.io.IOException;
import java.lang.String;
import java.io.*;
import java.util.*;


//esta clase  permite en el main hacer el parser por linea comando
public class Menu {

  @Parameter(names = "-i", description = "input", required = false)
  public String inputpath;

  @Parameter(names = "-d", description = "dict", hidden = true, required = false)
  public String dictpath;

  @Parameter(names = "-g", description = "ignoreddict", hidden = true, required = false)
  public String ignoreddictpath;

  @Parameter(names = "-o", description = "output", hidden = true, required = false)
  public String outputpath;

  @Parameter(names = "-r", description = "reverse", hidden = true, required = false)
  public Boolean reverse;

  @Parameter(names = "--input=", description = "inputfull", required = false)
  public String inputpath2;

  @Parameter(names = "--dictionary=", description = "dict", hidden = true, required = false)
  public String dictpath2;

  @Parameter(names = "--ignored=", description = "ignorededfictfull", hidden = true, required = false)
  public String ignoreddictpath2;

  @Parameter(names = "--output=", description = "outputfull", hidden = true, required = false)
  public String outputpath2;

  @Parameter(names = "--reverse", description = "reversefull", hidden = true, required = false)
  public Boolean reverse2;

  @Parameter(names = "-m", description = "interfaz", hidden = true, required = false)
  public Integer interfaz;

  @Parameter(names = "--implementation", description = "implementation", hidden = true, required = false)
  public Integer implementation;

  // funcion auxiliar
  private int notValid(Integer test) {
    int error = 0;
    if (test != null && test != 1 && test != 0) {
      error = 1;
    }
    return error;
  }

  // controla que el usuario haya ingresado input, y que en -m x  
  // o en --implementation x
  // x sea 1 o 0
  public int isError() {
    int error = 0;
    if (inputpath == null && inputpath2 == null) {
      error = 1;
    }
    else if(notValid(interfaz) + notValid(implementation) != 0 ) {
      error = 1 ;
    }
    return error;
  }


}
