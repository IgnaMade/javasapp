import com.beust.jcommander.*;
import java.io.FileReader;
import java.io.IOException;
import java.io.*;
import java.util.*;


public class Main {

  public static void main(String[] args) throws IOException {
    Mainhelper mainhelper = new Mainhelper();
    Menu settings = new Menu();
    int error = 0;
    // se obtiene los argumentos del usuario pasados por linea de comando
    try {
      new JCommander(settings, args); 
    } catch (Exception e) {
      error = 1;
      System.out.print( e) ;
    } // si hay un error en los argumentos ingresados por el usuario, fin ejecucion
     if (settings.isError()==1 || error == 1 ) {
      System.out.print("Exiting..\n") ;
    }
    else {
      // se inicializan y cargan todos los diccionarios y documetos a utilizar para relizar la traduccion
      mainhelper.readCommands(settings);
      Dict dictEsp = null;
      Dict dictIng = null;
      Dict dictEspaux = null;
      Dict dictIngaux = null;
      Ignored ignored = null;
      Document doc = null;
      Document translation = null;
      dictEsp = mainhelper.dictFromPath(1);
      dictIng = mainhelper.dictFromPath(0);
      ignored = mainhelper.ignoredFromPath();
      doc = mainhelper.inputFromPath();
      // si el documento ingresado por el usuario no existe, fin ejecucion
      if (doc == null) {
        System.out.print("Exiting..\n") ;
      } else { // sino se traduce
        // se obiene la interfaz
        int m = mainhelper.returnInterface();
        // se obtiene si se ingreso o no reverse
        int r = 0;
        r = mainhelper.returnReverse();
        // traductor
        Translator traductor = new Translator(dictEsp,dictIng,ignored,dictEspaux,dictIngaux);
        // doc traducido
        Document docTranslate = null ;
        // se traduce
        docTranslate = traductor.translate(doc,r,m);
        // se guardan los arhivos resultandes
        mainhelper.save(docTranslate,ignored,dictEsp,dictIng);
      }
    }
  }


}
