import java.io.*;
import java.util.*;
import java.util.ArrayList;
import java.io.FileWriter;


public class Translator {

  private Dict dictEI; // diccionario español-ingles
  private Dict dictIE; //diccionario ingles-español
  private Ignored ignored; // lista de palabras ignoradas
  private Dict dictEIaux; // diccionario español-ingles auxiliar
  private Dict dictIEaux; //diccionario ingles-español auxiliar


  public Translator(Dict dictesp, Dict dicting, Ignored ignoredl, Dict dictespaux, Dict dictingaux) {
    dictEI = dictesp;
    dictIE = dicting ;
    ignored = ignoredl;
    dictEIaux = dictespaux;
    dictIEaux = dictingaux ;
  }


  public Ignored ignoreddetraductor() {
    return ignored ;
  }


  public Dict dictespdetraductor() {
    return dictEI;
  }


  public Dict dictingdetraductor() {
    return dictIE;
  }


  // Pre: el primer y ultimo caracter de palabra son letras.
  public String translateWordInglish( String simple) { 
    String traduccion = "";
    Ignored dictignored = this.ignored ;
    Dict dicting = this.dictIE;
    Dict dictingaux = this.dictIEaux ;
    if ( true == dictignored.list().contains(simple)) {
      traduccion = simple;  // si simple esta en el dict de palabras ignoradas, ignorarla
    } else {

      if (true == dictingaux.find(simple)) { // si simpe esta en el traducir como
        traduccion = dictingaux.get(simple,0);
      } else {
        if (true == dicting.find(simple)) { // si siemple esta en el dict.txt
          traduccion = dicting.get(simple,0);
          //traduccion = dicting.listapalabras(dicting).get(simple);
        }
        if (traduccion == "" || false == dicting.find(simple)) {
          System.out.print("No hay traduccion para: " + simple + "\n");
          System.out.print("Ignorar (i) - Ignorar Todas (h) - Traducir como (t) - Traducir siempre como (s) \n");
          String letra = "";
          Scanner in = new Scanner(System.in);
          letra = in.next();
          if (0 == letra.compareTo("i")) {
            System.out.print("Palabra Ignorada una vez \n");
            traduccion = simple;
          } else if (0 == letra.compareTo("h")) {
            System.out.print("Palabra ignorada siempre \n");
            traduccion = simple;
            //dictignored.list(dictignored).add(simple);
            dictignored.add(simple);
          } else if (0 == letra.compareTo("t")) {
            System.out.print("traducir " + simple + " como: \n");
            traduccion = in.next();
            //agregar simple a dictingaux
            dictingaux.add(simple,traduccion);
          } else if (0 == letra.compareTo("s")) {
            System.out.print("traducir " + simple + " como: \n");
            traduccion = in.next();
            //agregar simple a dictingaux
            dictingaux.add(simple,traduccion);
            dicting.add(simple,traduccion);
            //agregar simple a dicting  guardandola con savedict  
          } else {
            System.out.print("Mal ingresada las opciones \n");
            //duda: se deberia salir del programa.. no creo, demasiado mala la user experience sino
          }
        }
      }
    }
    //// traducida simple
    return traduccion ;
  }


  public String translateEspanish(String simple) {
    String traduccion = "";
    Ignored dictignored = this.ignored ;
    Dict dictesp = this.dictEI;
    Dict dictespaux = this.dictEIaux ;
    if ( true == dictignored.list().contains(simple)) {
      traduccion = simple;  // si simple esta en el dict de palabras ignoradas, ignorarla
    } else {
      //dictespaux.listapalabras(dictespaux).containsKey(simple)
      if (true == dictespaux.find(simple)) { // si simpe esta en el traducir como
        traduccion = dictespaux.get(simple,1);
        //traduccion = dictespaux.listapalabras(dictespaux).get(simple);
      } else {
        if (true == dictesp.find(simple)) { // si siemple esta en el dict.txt
          //traduccion = dictesp.listapalabras(dictesp).get(simple);
          traduccion = dictesp.get(simple,1);
        }
        if (traduccion == "" || false == dictesp.find(simple)) {
          System.out.print("No hay traduccion para: " + simple + "\n");
          System.out.print("Ignorar (i) - Ignorar Todas (h) - Traducir como (t) - Traducir siempre como (s) \n");
          String letra = "";
          Scanner in = new Scanner(System.in);
          letra = in.next();
          if (0 == letra.compareTo("i")) {
            System.out.print("Palabra Ignorada una vez \n");
            traduccion = simple;
          } else if (0 == letra.compareTo("h")) {
            System.out.print("Palabra ignorada siempre \n");
            traduccion = simple;
            dictignored.add(simple);
          } else if (0 == letra.compareTo("t")) {
            System.out.print("traducir " + simple + " como: \n");
            traduccion = in.next();
            //agregar simple a dictespaux
            dictespaux.add(simple,traduccion);
          } else if (0 == letra.compareTo("s")) {
            System.out.print("traducir " + simple + " como: \n");
            traduccion = in.next();
            //agregar simple a dictespaux
            dictespaux.add(simple,traduccion);
            dictesp.add(simple,traduccion);
          } else {
            System.out.print("Mal ingresada las opciones \n");
            traduccion = simple ;
            //duda: se deberia salir del programa.. no creo, demasiado mala la user experience sino
          }
        }
      }
    }
    //// traducida simple
    return traduccion ;
  }


  // reccorre caracter por caracter la palabra y la traduce, modificando los diccionarios,
  // devuelve la palabra traducida, con los signos de puntuacion todo
  // interactua con el usuairio llamando a traducirpalabra
  public String translateWordComplete( String palabra, int r) {
    String traduccion = "";
    String simple = "";
    int a = 0 ;
    int b = 0;
    int c = 0;
    int d = 0;
    int n = palabra.length();
    //si el primer y ultimo caracter de la palabra no es simbolo  (tira un error en charat(0) debe ser mi compilador uso gcj) FUNCIONA este codigo
    if (Character.isLetterOrDigit(palabra.charAt(0)) && Character.isLetterOrDigit(palabra.charAt(n - 1))) {
      simple = palabra;
      a = 1;
    } else if (!Character.isLetterOrDigit(palabra.charAt(0)) && Character.isLetterOrDigit(palabra.charAt(n - 1))) {
      //traducir simple sin el primer caracter
      simple = palabra.substring(1, n);
      b = 1;
    } else if (Character.isLetterOrDigit(palabra.charAt(0)) && !Character.isLetterOrDigit(palabra.charAt(n - 1))) {
      //traducir simple sin el ultimo caracter
      simple = palabra.substring(0, n - 1);
      c = 1;
    } else if (!Character.isLetterOrDigit(palabra.charAt(0)) && !Character.isLetterOrDigit(palabra.charAt(n - 1))) {
      //traducir simple sin el primer y ultimo caracter
      simple = palabra.substring(1, n - 1);
      d = 1;
    }
    // se traduce de ingles a español
    if (r == 0) {
      traduccion = this.translateWordInglish(simple);
    } else {  // se traduce de español a ingles
      traduccion = this.translateEspanish(simple);
    }
    //aca se devuelve la traduccion con los signos de puntuacion agregados que pudiera tener agregados
    if (a == 1) {
      //se devuelve traduccion
      return (traduccion) ;
    } else if (b == 1) {
      //se devuelve traduccion
      return (palabra.charAt(0) + traduccion ) ;
    } else if (c == 1) {
      //se devuelve traduccion
      return (traduccion + palabra.charAt(n - 1));
    } else {  // d == 1
      //se devuelve traduccion
      return (palabra.charAt(0) + traduccion + palabra.charAt(n - 1));
    }
  }


  // pre: la oracion no tiene dos espacion contiguos ni dos enters contiguos, y un unico 
  // EOF al final del texto.  
  // reccorre caracter por caracter la oracion y la traduce, modificando los diccionarios,
  // devuelve oracion traducida, con los signos de puntuacion incluidos 
  // llama a translateWordComplete  
  public String translateLine(String oracion, int r) {
    String oraciontraducida = "";
    String palabraatraducir = "";
    String palabratraducida = "";
    int caracter = 0;
    int i = 0;
    while (i < oracion.length()) {
      caracter = (int)oracion.charAt(i) ;
      // -1 es EOF, 10 es enter, 32 es espacio
      // se termino de obtener una palabra a traducir
      if (caracter == -1 || caracter == 10 || caracter == 32) {
        // por si antes de EOF hay un enter o espacio
        if (palabraatraducir != "") { 
          // se traduce la palabra
          palabratraducida = this.translateWordComplete(palabraatraducir,r);
          // se agrega el ultimo caracter a la palabra traducida
          palabratraducida = palabratraducida + (char)caracter;
          // se agrega la palabra traducida a la oracion traducida
          oraciontraducida = oraciontraducida + palabratraducida;
          palabratraducida = "";
          palabraatraducir = "";
        }
      } else {  // se sigue formando la palabra a traducir
        palabraatraducir = palabraatraducir + (char)caracter ;
      }
      i = i + 1;
    }
    if (palabraatraducir != "") {
      palabratraducida = this.translateWordComplete( palabraatraducir, r);
      oraciontraducida = oraciontraducida + palabratraducida;
    }
    return oraciontraducida ;
  }


  // Devuelve un document que es la traduccion de un document pasado como
  // argumento, tal traduccion tiene el sentido dado por el parametro "r", 
  // y utiliza los diccionarios del parametro Traductor
  // si el document input tiene espacios contiguos o enters contiguos,
  // devuelve la traduccion pero con espaicios y enters correctamente formateados
  // es decir, sin haber dos contiguos
  public Document translate (Document input, int r, int m) {
    if (m == 1) {
      this.dictEIaux = new Dictmap();
      this.dictIEaux = new Dictmap();
    }
    else {
      this.dictEIaux = new Twolist();
      this.dictIEaux = new Twolist();
    }
    Document traduccion = new Document();
    String oraciontraducida = "" ;
    ArrayList<String> listaoraciones = new ArrayList<String>();
    listaoraciones = input.listOr();
    int i = 0;
    while (i < listaoraciones.size()) {
      oraciontraducida = this.translateLine(listaoraciones.get(i), r);
      traduccion.addOr(oraciontraducida);
      i++;
    }
    return traduccion ;
  }
}
