import java.io.*;
import java.util.*;

public class Ignored {


  private ArrayList<String> list;
  //constructor 


  public Ignored() {
    ArrayList<String> lista = new ArrayList<String>();
    list = lista;
  }


  // metodo que devuelve un diccionario de palabras ignoradas a partir
  // del path de un file de palabras ignoradas 
  public Ignored load(String path) {
    Ignored ignored = new Ignored();
    ArrayList<String> lista = new ArrayList<String>();
    ignored.list = lista;
    FileReader input = null;

    try {
      input = new FileReader(path);
      int caracter = input.read();
      String palabra = "";
      String palabra1 = "";
      int n = 0;
      while (caracter != -1) {
        //10 es enter -1 es EOF
        n = palabra.length();
        if (caracter == 10) {
          if (n > 1) {
            //System.out.println(palabra);
            palabra1 = palabra.substring(0, n - 1);
            //System.out.println(palabra1);
            ignored.list.add(palabra1);
            palabra = "";
            palabra1 = "";
          }
        } else {
          palabra = palabra + (char)caracter;
        }
        caracter = input.read();    
      }
    } catch (Exception e) {
      //e.printStackTrace();
      System.out.println("Ignored ingresado no encontrado, se usara un ignored vacio "); 
    } finally {
      // En el finally cerramos el fichero, para asegurarnos
      // que se cierra tanto si todo va bien como si salta 
      // una excepcion.
      try {                    
        if (null != input) {   
          input.close();     
        }                  
      } catch (Exception e2) { 
        e2.printStackTrace();
      }
    }
    return ignored;      
  }


  public void save( String path) {
    FileWriter output = null;
    try {   
      output = new FileWriter(path);
      Iterator<String> iter = list.iterator();
      while (iter.hasNext()) {
        String elemento = iter.next();
        //System.out.println(elemento+":" + "\n");
        output.write(elemento + ":" + "\n");
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      // En el finally cerramos el fichero, para asegurarnos
      // que se cierra tanto si todo va bien como si salta 
      // una excepcion.
      try {                    
        if (null != output) {   
          output.close();     
        }                  
      } catch (Exception e2) { 
        e2.printStackTrace();
      }
    } 
  }


  public void add(String palabra) {
    list.add(palabra);
  }


  public void print() {
    int i = 0;
    while (i < list.size()) {
      System.out.println(list.get(i));
      i++;
    }   
  }


  public ArrayList<String> list() {
    return list ;
  }
  
//para silenciar una queja del compilador, inutil.    
//private static final long serialVersionUID = 7526472295622776147L
// unique id
}
